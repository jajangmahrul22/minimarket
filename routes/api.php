<?php

use App\Models\Kasir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\API\KasirController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\API\BarangController;
use App\Http\Controllers\KategoriBarangController;
use App\Http\Controllers\RiwayatStokBarangController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'checkToken'], function() {
    Route::get('/kasir', [KasirController::class, 'index']);
    Route::get('/kasir/total', [KasirController::class, 'total_kasir']);
    Route::post('/kasir', [KasirController::class, 'store']);
    Route::get('/kasir/{kasir}', [KasirController::class, 'show']);
    Route::put('/kasir/{kasir}', [KasirController::class, 'update']);
    Route::delete('/kasir/{kasir}', [KasirController::class, 'destroy']);

    Route::get('/barang', [BarangController::class, 'index']);
    Route::get('/barang/total', [BarangController::class, 'total_barang']);
    Route::post('/barang', [BarangController::class, 'store']);
    Route::get('/barang/{barang}', [BarangController::class, 'show']);
    Route::put('/barang/{barang}', [BarangController::class, 'update']);
    Route::delete('/barang/{barang}', [BarangController::class, 'destroy']);

    Route::get('/kategori-barang/total', [KategoriBarangController::class, 'total_kategori']);
    Route::post('/kategori-barang', [KategoriBarangController::class, 'store']);
    Route::get('/kategori-barang/{kategori_barang}', [KategoriBarangController::class, 'show']);
    Route::put('/kategori-barang/{kategori_barang}', [KategoriBarangController::class, 'update']);
    Route::delete('/kategori-barang/{kategori_barang}', [KategoriBarangController::class, 'destroy']);

    Route::get('/supplier', [SupplierController::class, 'index']);
    Route::post('/supplier', [SupplierController::class, 'store']);
    Route::get('/supplier/total', [SupplierController::class, 'total_supplier']);
    Route::get('/supplier/{supplier}', [SupplierController::class, 'show']);
    Route::put('/supplier/{supplier}', [SupplierController::class, 'update']);
    Route::delete('/supplier/{supplier}', [SupplierController::class, 'destroy']);

    Route::get('/user', [UserController::class, 'index']);
    Route::post('/user', [UserController::class, 'store']);
    Route::put('/user/{user}', [UserController::class, 'update']);
    Route::get('/user/{user}', [UserController::class, 'show']);
    Route::delete('/user/{user}', [UserController::class, 'destroy']);

    Route::group(['prefix' => 'riwayat-stok-barang'], function() {
        Route::get('/', [RiwayatStokBarangController::class, 'index']);
        Route::post('/', [RiwayatStokBarangController::class, 'store']);
        Route::put('/{riwayat_stok}', [RiwayatStokBarangController::class, 'update']);  
    });

    
    Route::post('/add-transaksi', [TransaksiController::class, 'store']);  


    Route::post('/logout', [UserController::class, 'logout']);
});

Route::get('/kategori-barang', [KategoriBarangController::class, 'index']);
Route::middleware('checkGuestToken')->group(function(){
    Route::post('/login', [UserController::class, 'login']);
});

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
