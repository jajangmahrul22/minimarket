<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class RiwayatStokBarangFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stok_sebelum' => $this->faker->numberBetween(0, 100),
            'stok_terakhir' => $this->faker->numberBetween(0,100),
            'jenis' => $this->faker->randomElement(['penambahan', 'pengurangan']),
            'stok_update' => $this->faker->numberBetween(0, 100),
            'keterangan' => $this->faker->word(),
        ];
    }
}
