<?php

namespace Database\Factories;

use App\Models\Barang;
use App\Models\KategoriBarang;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class BarangFactory extends Factory
{
    protected $model = Barang::class;
    // protected $test = KategoriBarang::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'foto' => $this->faker->imageUrl(360, 360, 'animals', true, 'dogs', true, 'jpg'),
            'nama' => $this->faker->name(),
            'harga_beli' => $this->faker->numberBetween(1000, 10000),
            'harga_jual' => $this->faker->numberBetween(1000, 10000),
            'diskon' => $this->faker->numberBetween(0, 10000),
            'stok' => $this->faker->numberBetween(0, 100)
        ];
    }

    // public function kategori_barang()
    // {
    //     return [
    //         'nama_kategori' => $this->faker->word(),
    //     ];
    // }

}



// $factory->define(Barang::class, function (Faker $faker){
//         return [
//             'foto' => $faker->imageUrl(360, 360, 'animals', true, 'dogs', true, 'jpg'),
//             'nama' => $faker->word(),
//             'harga_beli' => $faker->numberBetween(1000, 10000),
//             'harga_jual' => $faker->numberBetween(1000, 10000),
//             'diskon' => $faker->numberBetween(0, 10000),
//         ];
// }); 

// $factory->define(KategoriBarang::class, function (Faker $faker){
//         return [
//             'nama_kategori' => $faker->word(),
//         ];
// });