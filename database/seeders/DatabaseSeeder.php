<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Barang;
use App\Models\Supplier;
use App\Models\Transaksi;
use Faker\Generator as Faker;
use App\Models\KategoriBarang;
use App\Models\DetailTransaksi;
use Illuminate\Database\Seeder;
use App\Models\RiwayatStokBarang;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        
        // Barang::create([
        //     'foto' => 'jajang.jpg',
        //     'nama' => 'rokok djarum',
        //     'harga_beli' => 3000,
        //     'harga_jual' => 12000,
        // ]);

        //Barang::factory(10)->create();
        // Supplier::factory(10)->create();

        // Supplier::factory(10)
        //     ->has(Barang::factory()
        //     ->has(KategoriBarang::factory()->count(10), 'kategori_barang')
        //     ->count(50), 'barang')
        //     ->create();

        // Supplier::factory()->has(
        //     Barang::factory()->has(
        //         KategoriBarang::factory()->count(3), 'kategori_barang'
        //     )->count(500)
        // )->count(10)->create();

        // User::factory(1)->create();
        User::create([
            'nama' => 'admin',
            'username' => 'admin',
            'password' => bcrypt('admin'), 
            'no_telp' => '0874682413927'
        ]);

        Supplier::factory()->has(
            Barang::factory(500)->has(
                RiwayatStokBarang::factory()->count(3), 'riwayat_stok_barang'
                ))
                ->count(10)
                ->create();

        KategoriBarang::factory()->has(
            Barang::factory(500)->has(
                RiwayatStokBarang::factory(3), 'riwayat_stok_barang'
                ))
                ->count(10)
                ->create();

        // Transaksi::factory()->has(
        //     DetailTransaksi::factory()->count(3), 'transaksi'
        // )->count(3)->create();



    }
}
