<?php

namespace App\Http\Middleware;

use Error;
use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            // cek ketersediaan token di cookie
            if(empty($_COOKIE['token'])) {
                throw new Exception('Token tidak valid');
            }

            $jwt = $_COOKIE['token'];

            // cek apakah token valid?
            $payload = JWT::decode($jwt, new Key(env('JWT_SECRET_KEY'), 'HS256'));
        } catch(Exception $e) {
            return ApiFormatter::createApi(404, 'Error', 'Token tidak valid');
        }

        // diarahkan ke proses selanjutnya
        setcookie('token', $jwt, time() + 1800, httponly:true);
        

        return $next($request);
    }
}
