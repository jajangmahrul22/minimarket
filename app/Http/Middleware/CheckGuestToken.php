<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Symfony\Component\Routing\Annotation\Route;

class CheckGuestToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        // 1. jika tidak ada cookie token maka bisa ke next
        // 2. jika ada cookies token yg valid maka tidak bisa ke next, return response anda tidak bisa mengunjungi halaman ini
        
        if(isset($_COOKIE['token'])) {
            $jwt = $_COOKIE['token'];

            if(JWT::decode($jwt, new Key(env('JWT_SECRET_KEY'), 'HS256'))) {
                return response()->json([
                    'code' => 403,
                    'status' => 'Not Authorized',
                    'message' => 'Anda tidak bisa mengunjungi halaman ini',
                    'data' => [
                        'prev_endpoint' => $request->url()
                    ]
                ]);
            }
        }

        return $next($request);

            


        // try{
        //     if(empty($_COOKIE['token'])) {
        //         //  return redirect()->route('total_barang')->with('message', 'Anda tidak bisa mengunjungi halaman ini');
        //         return $next($request);
        //     }
            
        //     $jwt = $_COOKIE['token'];
        //     // cek apakah token valid?
        //     $payload = JWT::decode($jwt, new Key(env('JWT_SECRET_KEY'), 'HS256'));
        //  }catch(Exception $e){
        //     return $e->getMessage();
        //  }

        //  setcookie("token", "", time() - 3600);
         

         // cek ketersediaan token di cookie
    //    try{
    //         if(empty($_COOKIE['token'])) {
    //             throw new Exception('Token tidak valid');
    //         }

    //         $jwt = $_COOKIE['token'];

    //         // cek apakah token valid?
    //         $payload = JWT::decode($jwt, new Key(env('JWT_SECRET_KEY'), 'HS256'));
    //    }catch(Exception $e){
    //        return response()->json([
    //         'code' => 403,
    //         'status' => 'Not Authorized',
    //         'message' => 'Anda tidak bisa mengunjungi halaman ini',
    //         'data' => [
    //             'prev_endpoint' => $request->url()
    //         ]
    //     ]);
    //         // return $e->getMessage();
    //         // exit;
    //    }

    //     // diarahkan ke proses selanjutnya
    //     setcookie('token', $jwt, time() + 300,  null, null, false, true);

    //     //return redirect()->route('api/login')->with('prev_endpoint', $request->url());
    //     // return redirect('api/login');
    //     return redirect()->back();
        // return $next($request);
    }
}
