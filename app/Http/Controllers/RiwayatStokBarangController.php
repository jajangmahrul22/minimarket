<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Barang;
use App\Helpers\ApiFormatter;
use Illuminate\Http\Response;
use App\Models\RiwayatStokBarang;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RiwayatStokBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $show = $request->show;
            $q = $request->q;
            if($show){
                $show = DB::table('barang')
                ->join('riwayat_stok_barang', 'riwayat_stok_barang.barang_id', '=', 'barang.id')
                ->select('barang.nama as nama_barang', 'riwayat_stok_barang.stok_sebelum', 
                'riwayat_stok_barang.stok_terakhir as stok_sesudah', 'riwayat_stok_barang.stok_update', 
                'riwayat_stok_barang.jenis','riwayat_stok_barang.keterangan',
                'riwayat_stok_barang.created_at')
                ->where('riwayat_stok_barang.id', 'like', "%{$q}%")
                ->orWhere('barang.nama', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_sebelum', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_terakhir', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_update', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.jenis', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.keterangan', 'like', "%{$q}%")
                ->orderBy('riwayat_stok_barang.created_at', 'desc')
                ->paginate($show, ['*'], 'p' );
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            }else {
                $data = DB::table('barang')
                ->join('riwayat_stok_barang', 'riwayat_stok_barang.barang_id', '=', 'barang.id')
                ->select('riwayat_stok_barang.id','barang.nama as nama_barang', 'riwayat_stok_barang.stok_sebelum', 
                'riwayat_stok_barang.stok_terakhir as stok_sesudah', 'riwayat_stok_barang.stok_update', 
                'riwayat_stok_barang.jenis','riwayat_stok_barang.keterangan',
                'riwayat_stok_barang.created_at')
                ->where('riwayat_stok_barang.id', 'like', "%{$q}%")
                ->orWhere('barang.nama', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_sebelum', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_terakhir', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.stok_update', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.jenis', 'like', "%{$q}%")
                ->orWhere('riwayat_stok_barang.keterangan', 'like', "%{$q}%")
                ->orderBy('riwayat_stok_barang.created_at', 'desc')
                ->paginate(5, ['*'], 'p' );
                return ApiFormatter::createApi(200, 'OK', 'sukses', $data);
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (Exception $e){
            // return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi
         $validator = Validator::make($request->all(),[
            'barang_id' => 'required',
            'jenis' => 'required|in:penambahan,pengurangan',
            // 'stok_sebelum' => 'nullable',
            'stok_update' => 'required',
            // 'stok_terakhir' => 'nullable',
            'keterangan' => 'nullable',

        ], [
            'barang_id.required' => 'Barang_id wajib diisi',
            'jenis.required' => 'Jenis wajib diisi',
            'jenis.in' => 'Jenis hanya diisi penambahan atau pengurangan',
            // 'stok_sebelum.required' => 'Stok sebelum wajib diisi',
            'stok_update.required' => 'Stok update wajib diisi',
            // 'stok_terakhir.required' => 'Stok terakhir wajib diisi',
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

      

        try {
            DB::beginTransaction();
            $riwayatStokBarangs = [
                'barang_id' =>$request->barang_id,
            ];
            $barang = Barang::find($riwayatStokBarangs['barang_id']);
            
            if(!$barang){
                DB::rollback();
                return ApiFormatter::createApi(404, 'Not found', 'Barang_id tidak ditemukan');
            }
            

            $stok_sebelum = $barang->stok;
            $jenis = $request->jenis;
            $stok_update = $request->stok_update;
            $keterangan = $request->keterangan;
            $stok_terakhir = 0;
            
            if($jenis == 'penambahan'){
                $stok_terakhir = 0 ? 0 : $stok_update + $stok_sebelum;
            } else if($jenis == 'pengurangan'){
                $stok_terakhir = 0 ? 0 : $stok_sebelum - $stok_update;
            } 

            $riwayatStokBarangs = RiwayatStokBarang::create([
                'barang_id' => $request->barang_id,
                'stok_sebelum' => $stok_sebelum,
                'stok_terakhir' => $stok_terakhir,
                'jenis' => $jenis,
                'stok_update' => $stok_update,
                'keterangan' => $keterangan
            ]);
            // $riwayatStokBarangs = RiwayatStokBarang::create($riwayatStokBarangs);
            
            // $riwayatStokBarangs->update([
            //     'stok_sebelum' => $stok_sebelum,
            //     'stok_update' => $stok_update,
            //     'jenis' => $jenis,
            //     'stok_terakhir' => $stok_terakhir,
            //     'keterangan' => $keterangan
            // ]);

            $riwayatStokBarangs = RiwayatStokBarang::find($riwayatStokBarangs->id);

            DB::commit();
            if($stok_terakhir > 1){
                DB::table('barang')->where('id', '=', $barang->id)
            ->update(['stok' => $stok_terakhir]);}
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (Exception $e) {
            DB::rollback();
            return  $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $riwayatStokBarangs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try{
            DB::beginTransaction();
            $riwayats = RiwayatStokBarang::findOrFail($id);
            
            $keterangan = ($request->keterangan == null) ? $riwayats->keterangan : $request->keterangan;

            $riwayats->update([
                'keterangan' => $keterangan
            ]);
            DB::commit();
        }catch(ModelNotFoundException $e){
            return ApiFormatter::createApi(404, 'Not Found', 'Data tidak ditemukan');
        }catch(\Exception $e){
            DB::rollback();
            return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! ada yang tidak beres');
        }
        
        return ApiFormatter::createApi(200, 'OK', 'Sukses', $riwayats);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
