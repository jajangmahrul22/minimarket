<?php

namespace App\Http\Controllers\API;

use App\Models\Kasir;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class KasirController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function total_kasir(){
        try{
            // $kasir = DB::table('kasir')
            // ->select(DB::raw('count(id) as total_data_kasir'))->get();
            $kasir = Kasir::count();
        }catch (\Exception $e){
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        return ApiFormatter::createApi(200, 'OK', 'sukses', $kasir);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        try{
            $show = $request->show;
            $q = $request->q;
            if($show){
                $show = Kasir::select(['id', 'foto', 'nama','tgl_mulai_bekerja', 'created_at', 'updated_at'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('tgl_mulai_bekerja', 'like', "%{$q}%")
                ->orderBy('id', 'desc')                
                ->paginate($show, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            } else{
                $data = Kasir::select(['id', 'foto', 'nama','tgl_mulai_bekerja', 'created_at', 'updated_at'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('tgl_mulai_bekerja', 'like', "%{$q}%") 
                ->orderBy('id', 'desc')            
                ->paginate(5, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $data);   
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e) {
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
         }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'foto' => 'sometimes|nullable|mimes:jpg,jpeg,png|max:2048',
            'email' => 'required|email:rfc,dns|unique:kasir,email',
            'umur' => 'required|integer|min:1',
            'alamat' => 'required',
            'no_hp' => 'required|numeric',
            'status' => 'required|in:lajang,kawin',
            'tanggal_mulai_bekerja' => 'required|date_format:Y-m-d'
 
        ], [
            'email.unique' => 'Email sudah digunakan',
            'email.required' => 'Email wajib diisi',
            'email.email' => 'Format email tidak valid',
            'foto.mimes' => 'Ekstensi foto harus jpg, jpeg, png',
            'foto.max' => 'Ukuran foto tidak boleh lebih dari 2Mb',
            'nama.required' => 'Nama wajib diisi',
            'alamat.required' => 'Alamat wajib diisi',
            'no_hp.required' => 'No Hp wajib diisi',
            'no_hp.numeric' => 'No Hp harus berupa angka',
            'umur.required' => 'Umur wajib diisi',
            'umur.integer' => 'Umur harus berupa angka',
            'umur.min' => 'Format umur salah',
            'status.required' => 'Status wajib diisi',
            'status.in' => 'Status hanya diisi lajang atau kawin',
            'tanggal_mulai_bekerja.required' => 'Tanggal mulai bekerja wajib diisi',
            'tanggal_mulai_bekerja.date_format' => 'Format tanggal harus Y-m-d'
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

        if (!$request->filled('no_hp')) {
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp wajib diisi']]);
        }
       
        $noHp = $request->input('no_hp');
        /* if(substr($noHp, 0, 1) !== '0' && strlen($noHp) < 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => 'No Hp harus terdiri dari 12 angka dan dimulai dari angka 0']);
        }else */ 
        if(substr($noHp, 0, 1) !== '0'){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus dimulai dengan angka 0']]);
        }else if(strlen($noHp) < 12 ||strlen($noHp) > 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus terdiri dari 12 angka']]);
        }

        

        // $noHPValidasi = Validator::make($request->all(), ['no_hp' => 'regex:/^0[0-9]{11}$/'],
        //                 ['no_hp.regex' => 'No Hp harus terdiri dari 12 angka']);

        // if ($validator->fails() || $noHPValidasi->fails()) {
        //     $errors = $validator->errors();
        //     $noHPErrors = $noHPValidasi->errors();

        //     if ($noHPErrors->has('no_hp')) {
        //         $errors->add('no_hp', $noHPErrors->first('no_hp'));
        //     }

        //     return ApiFormatter::createApi(400, 'Error', $errors->all());
        //     // return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        // }

        // insert to table
        try {
            // upload photo
            if($request->hasFile('foto')){
                $foto = $request->file('foto');
                   
                // $foto = $request->foto;
                $nama_foto = uniqid().'.'.$foto->getClientOriginalExtension();
                Storage::disk('public')->putFileAs('img', $foto, $nama_foto);
                $foto = Storage::disk('public')->url('img/'.$nama_foto);
            }else{
                $foto = 'https://source.unsplash.com/random/200x200';
            }
         

            $kasir = Kasir::create([
                'foto' => $foto,
                'nama' => $request->nama,
                'umur' => $request->umur,
                'no_hp' => $request->no_hp,
                'email' => $request->email,
                'alamat' => $request->alamat,
                'status' => $request->status,
                'tgl_mulai_bekerja' => $request->tanggal_mulai_bekerja
            ]);
        } catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch (\Exception $e) {
            // return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        // response
        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $kasir);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $kasir = Kasir::findOrFail($id);
        } catch(\Exception $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $kasir);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi
        // $validator = Validator::make($request->all(), [
        //     'foto' => 'nullable|mimes:jpg,jpeg,png|max:2048',
        //     'nama' => 'nullable',
        //     'umur' => 'nullable|integer|min:1',
        //     'alamat' => 'nullable',
        //     'no_hp' => 'nullable|numeric|min:0|digits_between:1,12',
        //     'status' => 'nullable|in:lajang,kawin',
        //     'tanggal_mulai_bekerja' => 'nullable|date|date_format:Y-m-d'
 
        // ], [
        //     'foto.mimes' => 'Ekstensi foto harus jpg, jpeg, png',
        //     'foto.max' => 'Ukuran foto tidak lebih dari 2Mb',
        //     'nama.required' => 'Nama wajib diisi',
        //     'no_hp.required' => 'No Hp wajib diisi',
        //     'no_hp.numeric' => 'No Hp wajib berupa angka',
        //     'no_hp.min' => 'Format No Hp salah',
        //     'no_hp.digits_between' => 'No Hp melebihi 12 angka',
        //     'umur.integer' => 'Umur harus berupa angka',
        //     'umur.min' => 'Format umur salah',
        //     'status.in' => 'Status hanya diisi lajang atau kawin',
        //     'tanggal_mulai_bekerja.date_format' => 'Format tanggal kurang lengkap',
        //     'tanggal_mulai_bekerja.date' => 'Tanggal mulai bekerja harus berupa format tanggal'
        // ]);

        // if ($validator->fails()) {
        //     return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        // }

        try {
        $kasir = Kasir::findOrFail($id);

        if($request->hasFile('foto')){
            $validator = Validator::make($request->all(), [
                'foto' => 'nullable|mimes:jpg,jpeg,png|max:2048'
                ], [
                    'foto.mimes' => 'Ekstensi foto harus jpg, jpeg, png',
                    'foto.max' => 'Ukuran foto tidak lebih dari 2Mb',
                ]);

                if ($validator->fails()) {
                    return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
                }

            $foto = $request->file('foto');

            $fotoLama = basename($kasir->foto);
            $path_foto = public_path("storage/img/{$fotoLama}");
            if(file_exists($path_foto)){
                unlink($path_foto);
            }
         
            $nama_foto = uniqid().'.'.$foto->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('img', $foto, $nama_foto);
            $foto = Storage::disk('public')->url('img/'.$nama_foto);
        }else{
            $foto = $kasir->foto;
        }

        $nama = $request->filled('nama') ? $request->nama : $kasir->nama;
        $umur = $request->filled('umur') ? $request->validate(['umur' => 'integer|min:1'],
                ['umur.integer' => 'Umur harus berupa angka', 'umur.min' => 'Format umur salah',])
                ['umur'] : $kasir->umur;
        $no_hp = $request->filled('no_hp') ? $request->validate(['no_hp' => 'nullable|numeric'],[
          'no_hp.numeric' => 'No Hp harus berupa angka'  
        ])
        ['no_hp'] : $kasir->no_hp;
       /*  if(substr($no_hp, 0, 1) !== '0' && strlen($no_hp) < 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => 'No Hp harus terdiri dari 12 angka dan dimulai dari angka 0']);
        }else */ 
        if(substr($no_hp, 0, 1) !== '0'){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus dimulai dengan angka 0']]);
        }else if(strlen($no_hp) < 12 || strlen($no_hp) > 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus terdiri dari 12 angka']]);
        }
        $alamat = $request->filled('alamat') ? $request->alamat : $kasir->alamat;
        $status = $request->filled('status') ? $request->validate(['status' => 'in:lajang,kawin'],
                ['status.in' => 'Status hanya diisi lajang atau kawin'])
                ['status'] : $kasir->status;
        $email = $request->filled('email') ? $request->validate(['email' => 'email:rfc,dns|unique:kasir,email,'.$kasir->id],
                [ 'email.unique' => 'Email sudah digunakan',
                    'email.email' => 'Format email tidak valid'
                ])
                ['email'] : $kasir->email;
        $tanggal_mulai_bekerja = $request->filled('tanggal_mulai_bekerja') ? $request->validate(['tanggal_mulai_bekerja' => 'date_format:Y-m-d'],
                ['tanggal_mulai_bekerja.date_format' => 'Format tanggal harus Y-m-d'
                ])
                ['tanggal_mulai_bekerja'] : $kasir->tgl_mulai_bekerja;
       
        $kasir->update(compact('foto', 'nama', 'umur', 'no_hp', 'alamat', 'status', 'email', 'tanggal_mulai_bekerja'));

        // if($request->nama !== null){
        //     $nama = $request->nama;
        // }else{
        //     $nama = $kasir->nama;
        // }

        // if($request->umur !== null){
        //     $validator = Validator::make($request->all(), [
        //         'umur' => 'nullable|integer|min:1'
        //         ], [
        //         'umur.integer' => 'Umur harus berupa angka',
        //         'umur.min' => 'Format umur salah'
        //         ]);

        //         if ($validator->fails()) {
        //             return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        //         }

        //     $umur = $request->umur;
        // }else{
        //     $umur = $kasir->umur;
        // }

        // if($request->no_hp !== null){
        //     $validator = Validator::make($request->all(), [
        //         'no_hp' => 'nullable|numeric|min:0|digits_between:1,12'
        //         ], [
        //         'no_hp.required' => 'No Hp wajib diisi',
        //         'no_hp.numeric' => 'No Hp wajib berupa angka',
        //         'no_hp.min' => 'Format No Hp salah',
        //         'no_hp.digits_between' => 'No Hp melebihi 12 angka',
        //         ]);

        //         if ($validator->fails()) {
        //             return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        //         }

        //     $no_hp = $request->no_hp;
        // }else{
        //     $no_hp = $kasir->no_hp;
        // }

        // if($request->alamat !== null){
        //     $alamat = $request->alamat;
        // }else{
        //     $alamat = $kasir->alamat;
        // }

        // if($request->status !== null){
        //     $validator = Validator::make($request->all(), [
        //         'status' => 'nullable|in:lajang,kawin'
        //         ], [
        //         'status.in' => 'Status hanya diisi lajang atau kawin',
        //         ]);

        //         if ($validator->fails()) {
        //             return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        //         }

        //     $status = $request->status;
        // }else{
        //     $status = $kasir->status;
        // }

        // if($request->tanggal_mulai_bekerja !== null){
        //     $validator = Validator::make($request->all(), [
        //         'tanggal_mulai_bekerja' => 'nullable|date|date_format:Y-m-d'
        //         ], [
        //             'tanggal_mulai_bekerja.date_format' => 'Format tanggal kurang lengkap',
        //             'tanggal_mulai_bekerja.date' => 'Tanggal mulai bekerja harus berupa format tanggal'
        //         ]);

        //         if ($validator->fails()) {
        //             return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        //         }

        //     $tanggal_mulai_bekerja = $request->tanggal_mulai_bekerja;
        // }else{
        //     $tanggal_mulai_bekerja = $kasir->tanggal_mulai_bekerja;
        // }

        // if($request->has('email')){
        //     if($request->email !== $kasir->email){
        //         $validator = Validator::make($request->all(), [
        //         'email' => 'email:rfc,dns|regex:/@gmail\.com$/i|unique:kasir,email'
        //         ], [
        //             'email.unique' => 'Email sudah digunakan',
        //             'email.email' => 'Format email tidak valid',
        //             'email.regex' => 'Email harus menggunakan "@gmail.com"'
        //         ]);

        //         if ($validator->fails()) {
        //             return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        //         }

        //     $email = $request->email;
        //     }else{
        //         $email = $kasir->email;
        //     }
        // }else{
        //   $email = $kasir->email;
        // }

        // $nama = $request->has('nama') ? $request->nama : $kasir->nama;
        // $umur = $request->has('umur') ? $request->umur : $kasir->umur;
        // $no_hp = $request->has('no_hp') ? $request->no_hp : $kasir->no_hp;
        // $alamat = $request->has('alamat') ? $request->alamat : $kasir->alamat;
        // $status = $request->has('status') ? $request->status : $kasir->status;
        // $tanggal_mulai_bekerja = $request->has('tanggal_mulai_bekerja') ? $request->tanggal_mulai_bekerja : $kasir->tanggal_mulai_bekerja;
        
        // if ($foto != null){
        //     $fotoLama = $kasir->foto;
        //     $fotoLama = explode('/', $fotoLama);
        //     $fotoLama = $fotoLama[5];
        //     $path_foto = public_path('storage/img/'.$fotoLama);
        //     if(file_exists($path_foto)){
        //         unlink($path_foto);
        //     }

        //     $nama_foto = uniqid().'.'.$foto->getClientOriginalExtension();
        //     Storage::disk('public')->putFileAs('img', $foto, $nama_foto);
        //     $foto = Storage::disk('public')->url('img/'.$nama_foto);
        // }else{
        //     $foto = $kasir->foto;
        // }

        // $kasir->update([
        //     'foto' => $foto,
        //     'nama' => $nama,
        //     'umur' => $umur,
        //     'no_hp' => $no_hp,
        //     'email' => $email,
        //     'alamat' => $alamat,
        //     'status' => $status,
        //     'tgl_mulai_bekerja' => $tanggal_mulai_bekerja
        // ]);
        } catch(ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch(ValidationException $e){
            return ApiFormatter::createApi(400, 'Error', $e->errors());
        }catch (\Exception $e){
          //  return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $kasir);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // algoritma hapus data kasir
        // 1. cek terlebih dahulu apakah ada foto milik kasir yg di simpan ke dalam folder ?
        // jika ada maka hapus foto tersebut
        // 2. hapus data aksir yg ada di db
        // 3. kirim response

        try {
            // get kasir by id
            $kasir = Kasir::findOrFail($id);
            // get foto kasir (url)
            $foto = $kasir->foto;
            // get only nama file foto
            $foto = explode('/', $foto);
            $foto = $foto[5];
            // cek apakah file tersebut ada ?
            $path_foto = public_path('storage/img/'.$foto);
            if(file_exists($path_foto)) {
                unlink($path_foto);
            }
            // hapus data kasir
            $kasir->delete();
        } catch(ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch(\Exception $e) {
            return ApiFormatter::createApi(500, 'Error', 'Data gagal dihapus');
        }

        return ApiFormatter::createApi(200, 'OK', 'Sukses menghapus data');        
    }
}
