<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\Barang;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use App\Models\KategoriBarang;
use App\Models\RiwayatStokBarang;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class BarangController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function total_barang(){
        try{
            // $barang = DB::table('barang')
            // ->select(DB::raw('count(id) as total_barang'))->get();
            $barang = Barang::count();
        }catch (\Exception $e){
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        return ApiFormatter::createApi(200, 'OK', 'sukses', $barang);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $show = $request->show;
            $q = $request->q;
            $kategori = $request->kategori;
            $supplier = $request->supplier;
            if($show){
                $show = DB::table('barang')
                ->leftJoin('kategori_barang', 'kategori_barang.id', '=', 'barang.kategori_barang_id')
                ->leftJoin('supplier', 'supplier.id', '=', 'barang.supplier_id')
                ->select('barang.id', 'kategori_barang.nama_kategori', 'supplier.nama as nama_supplier', 'barang.foto', 'barang.nama',
                'barang.harga_jual', 'barang.harga_beli', 'barang.diskon', 'barang.stok', 'barang.created_at',
                'barang.updated_at')
                ->where('barang.id', 'like', "%{$q}%")
                ->orWhere('barang.nama', 'like', "%{$q}%")
                ->orWhere('barang.harga_jual', 'like', "%{$q}%")
                ->orWhere('barang.harga_beli', 'like', "%{$q}%")
                ->orWhere('barang.diskon', 'like', "%{$q}%")
                ->orderBy('barang.id', 'desc')
                ->paginate($show, ['*'], 'p' );
                // $show = Barang::select(['id', 'foto', 'nama', 'harga_jual', 'harga_beli', 'diskon', 'created_at', 'updated_at'])
                //         ->where('nama', 'like', "%{$q}%")
                //         ->orWhere('id', 'like', "%{$q}%")
                //         ->orWhere('harga_jual', 'like', "%{$q}%")
                //         ->orWhere('harga_beli', 'like', "%{$q}%")
                //         ->orWhere('diskon', 'like', "%{$q}%")
                //         ->orderBy('id', 'desc') 
                //         ->paginate($show, ['*'], 'p' );
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            }else if($kategori){
                $dKategori = DB::table('barang')
                ->leftJoin('kategori_barang', 'kategori_barang.id', '=', 'barang.kategori_barang_id')
                ->leftJoin('supplier', 'supplier.id', '=', 'barang.supplier_id')
                ->where('kategori_barang.id', 'like', "%{$kategori}%")
                ->orWhere('barang.nama', 'like', "%{$q}%")
                ->orWhere('barang.harga_jual', 'like', "%{$q}%")
                ->orWhere('barang.harga_beli', 'like', "%{$q}%")
                ->orWhere('barang.diskon', 'like', "%{$q}%")
                ->orderBy('kategori_barang.id', 'desc')
                ->select('barang.id', 'kategori_barang.nama_kategori', 'supplier.nama as nama_supplier', 'barang.foto', 'barang.nama',
                'barang.harga_jual', 'barang.harga_beli', 'barang.diskon', 'barang.stok', 'barang.created_at',
                'barang.updated_at')
                ->paginate(5, ['*'], 'p' );
                return ApiFormatter::createApi(200, 'OK', 'sukses', $dKategori);
            }else if($supplier){
                $dSupplier = DB::table('barang')
                ->leftJoin('kategori_barang', 'kategori_barang.id', '=', 'barang.kategori_barang_id')
                ->leftJoin('supplier', 'supplier.id', '=', 'barang.supplier_id')
                ->where('supplier.id', 'like', "%{$supplier}%")
                ->orWhere('supplier.nama', 'like', "%{$supplier}%")
                ->orderBy('supplier.id', 'desc')
                ->select('barang.id', 'kategori_barang.nama_kategori', 'supplier.nama as nama_supplier', 'barang.foto', 'barang.nama',
                'barang.harga_jual', 'barang.harga_beli', 'barang.diskon', 'barang.stok', 'barang.created_at',
                'barang.updated_at')
                ->paginate(5, ['*'], 'p' );
                return ApiFormatter::createApi(200, 'OK', 'sukses', $dSupplier);
            }else {
                $data = DB::table('barang')
                ->leftJoin('kategori_barang', 'kategori_barang.id', '=', 'barang.kategori_barang_id')
                ->leftJoin('supplier', 'supplier.id', '=', 'barang.supplier_id')
                ->where('kategori_barang.id', 'like',"%{$kategori}%")
                ->orWhere('barang.nama', 'like', "%{$q}%")
                ->orWhere('barang.id', 'like', "%{$q}%")
                ->orWhere('barang.harga_jual', 'like', "%{$q}%")
                ->orWhere('barang.harga_beli', 'like', "%{$q}%")
                ->orWhere('barang.diskon', 'like', "%{$q}%")
                ->orderBy('barang.id', 'desc')
                ->select('barang.id', 'kategori_barang.nama_kategori', 'supplier.nama as nama_supplier', 'barang.foto', 'barang.nama',
                'barang.harga_jual', 'barang.harga_beli', 'barang.diskon', 'barang.stok', 'barang.created_at',
                'barang.updated_at');
                $data = $data->paginate(5, ['*'], 'p' );
               
               return ApiFormatter::createApi(200, 'OK', 'sukses', $data);
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e){
            return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        } 
        
    }
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi
        $validator = Validator::make($request->all(),[
            'supplier_id' => 'required|min:1',
            'kategori_barang_id' => 'required|min:1',
            'nama' => 'required',
            'foto' => 'nullable|mimes:jpg,jpeg,png|max:2048',
            'harga_beli' => 'required|integer|min:1',
            'harga_jual' => 'required|integer|min:1',
            'stok' => 'required|min:1',
            'diskon' => 'nullable|min:1',
        ], [
            'supplier_id.required' => 'Supplier_id wajib diisi',
            'supplier_id.min' => 'Supplier_id harus angka positif',
            'kategori_barang_id.required' => 'Kategori_barang_id wajib diisi',
            'kategori_barang_id.min' => 'Kategori_barang_id harus angka positif',
            'nama.required' => 'Nama wajib diisi',
            'foto.mimes' => 'Ekstensi foto harus jpg, jpeg, png',
            'foto.max' => 'Ukuran foto tidak boleh lebih dari 2Mb',
            'harga_beli.required' => 'Harga beli wajib diisi',
            'harga_beli.integer' => 'Harga beli harus berupa angka',
            'harga_beli.min' => 'Harga beli harus angka positif',
            'harga_jual.required' => 'Harga jual wajib diisi',
            'harga_jual.integer' => 'Harga jual harus berupa angka',
            'harga_jual.min' => 'Harga jual harus angka positif',
            'diskon.min' => 'Diskon harus angka positif',
            'stok.min' => 'Stok harus angka positif',
            'stok.required' => 'Stok wajib diisi',
            // 'jenis.in' => 'Jenis hanya diisi penambahan atau pengurangan',
            // 'stok_terakhir.nullable' => 'Stok_terakhir harus diisi'
            
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

        try {
            DB::beginTransaction();
            // upload photo
            if($request->hasFile('foto')){
                $foto = $request->file('foto');
                $nama_foto = uniqid().'.'.$foto->getClientOriginalExtension();
                Storage::disk('public')->putFileAs('img/barang/', $foto, $nama_foto);
                $foto = Storage::disk('public')->url('img/barang/'.$nama_foto);
            }else{
                $foto = 'https://source.unsplash.com/random/200x200';
            }
           
            $harga_beli = $request->harga_beli;
            $harga_jual = $request->harga_jual;
            if ($harga_jual <= $harga_beli) {
                return ApiFormatter::createApi(400, 'Bad Request', ['harga_jual' => ['Harga jual harus lebih besar dari harga beli']]);
            }
            
            $diskon = $request->diskon;
            
            if ($diskon >= $harga_jual) {
                return ApiFormatter::createApi(400, 'Bad Request',['diskon' => ['Diskon harus lebih kecil dari harga jual']]);
            }

            $barangRequest = [
                'foto' => $foto,
                'nama' => $request->nama,
                'harga_beli' => $harga_beli,
                'harga_jual' => $harga_jual,
                'diskon' => $diskon,
                'stok' => $request->stok
            ];
            $barang = Barang::create($barangRequest);
            
            $supplier_id = Supplier::find($request->supplier_id);
            $kategori_barang_id = KategoriBarang::find($request->kategori_barang_id);
            
            /* if (!$kategori_barang_id && !$supplier_id) {
                return ApiFormatter::createApi(400, 'Bad Request', 'kategori_barang_id dan supplier_id tidak ditemukan');
            } else */ 
            if (!$supplier_id) {
                return ApiFormatter::createApi(400, 'Bad Request', ['supplier_id' => ['supplier_id tidak ditemukan']]);
            }else if (!$kategori_barang_id) {
                return ApiFormatter::createApi(400, 'Bad Request', ['kategori_barang_id' => ['kategori_barang_id tidak ditemukan']]);
            }

            $barang->update([
                'supplier_id' => $supplier_id->id,
                'kategori_barang_id' => $kategori_barang_id->id,
            ]); 

            $stok_sebelum = $barang->stok;
            // $jenis = $request->jenis == null ? 'penambahan' : $request->jenis;
            // $stok_update = $request->stok_update == null ? 0 : $request->stok_update;
            // $stok_terakhir = 0;
            $count = Barang::count();
            $keterangan = ($count == 1) ? 'barang pertama masuk' : null;
            

            // if($jenis == 'penambahan'){
            //     $stok_terakhir = 0 ? 0 : $stok_update + $stok_sebelum;
            // } else if($jenis == 'pengurangan'){
            //     $stok_terakhir = 0 ? 0 : $stok_sebelum - $stok_update;
            // }           

            RiwayatStokBarang::create([
                'barang_id' => $barang->id,
                'stok_sebelum' => $stok_sebelum,
                'stok_terakhir' => $stok_sebelum,
                'jenis' => 'penambahan',
                'stok_update' => 0,
                'keterangan' => $keterangan
            ]);
            // if($stok_terakhir > 1){
            //     DB::table('barang')->where('id', '=', $barang->id)
            // ->update(['stok' => $stok_terakhir]);}
            $barangs = Barang::find($barang->id);
        }catch (Exception $e) {
            DB::rollback();
            return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        DB::commit();
        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $barangs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $show = Barang::findOrFail($id);
            // $show = DB::table('barang')
            // ->leftJoin('kategori_barang', 'kategori_barang.id', '=', 'barang.kategori_barang_id')
            // ->leftJoin('supplier', 'supplier.id', '=', 'barang.supplier_id')
            // ->select('kategori_barang.nama_kategori', 'supplier.nama as nama_supplier', 'barang.nama', 'barang.id', 'barang.foto',
            // 'barang.harga_jual', 'barang.harga_beli', 'barang.diskon', 'barang.created_at',
            // 'barang.updated_at')
            // ->where('barang.id', 'like', "%{$id_barang}%")
            // ->orderBy('barang.id', 'desc')->get();
        }catch (\Exception $e){
            // return $e->getMessage();
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        try {
            DB::beginTransaction();
            $barang = Barang::findOrFail($id);

            if($request->hasFile('foto')){
                $validator = Validator::make($request->all(), [
                    'foto' => 'nullable|mimes:jpg,jpeg,png|max:2048',
                    ], [
                        'foto.mimes' => 'Ekstensi foto harus jpg, jpeg, png',
                        'foto.max' => 'Ukuran foto tidak lebih dari 2Mb',
                    ]);
    
                    if ($validator->fails()) {
                        return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
                    }

                    $foto = $request->file('foto');

                    $fotoLama = basename($barang->foto);
                    $path_foto = public_path("storage/img/{$fotoLama}");
                    if(file_exists($path_foto)){
                        unlink($path_foto);
                    }

                    $nama_foto = uniqid().'.'.$foto->getClientOriginalExtension();
                    Storage::disk('public')->putFileAs('img', $foto, $nama_foto);
                    $foto = Storage::disk('public')->url('img/'.$nama_foto);
            }else{
                $foto = $barang->foto;
            }
            
            $supplier_id = Supplier::find($request->filled('supplier_id') ? $request->supplier_id : $barang->supplier_id);
            $kategori_barang_id = KategoriBarang::find($request->filled('kategori_barang_id') ? $request->kategori_barang_id : $barang->kategori_barang_id);
            
            /* if (!$kategori_barang_id && !$supplier_id) {
                return ApiFormatter::createApi(400, 'Bad Request', 'kategori_barang_id dan supplier_id tidak ditemukan');
            } else */ 
            if (!$supplier_id) {
                return ApiFormatter::createApi(400, 'Bad Request', ['supplier_id' => ['supplier_id tidak ditemukan']]);
            }else if (!$kategori_barang_id) {
                return ApiFormatter::createApi(400, 'Bad Request', ['kategori_barang_id' => ['kategori_barang_id tidak ditemukan']]);
            }

            $supplier_id = $supplier_id->id;
            $kategori_barang_id = $kategori_barang_id->id ;

            
            $nama = $request->filled('nama') ? $request->validate(['nama' => 'required'],
                    ['nama.required' => 'Nama wajib diisi'])
                    ['nama'] : $barang->nama;

            $harga_beli = $request->filled('harga_beli') ? $request->validate(['harga_beli' => 'min:1'],
                        ['harga_beli.min' => 'Harga beli harus angka positif'])
                        ['harga_beli'] : $barang->harga_beli;
            $harga_jual = $request->filled('harga_jual') ? $request->validate(['harga_jual' => 'min:1'],
                        ['harga_jual.min' => 'Harga jual harus angka positif'])
                        ['harga_jual'] : $barang->harga_jual;
            if ($harga_jual <= $harga_beli) {
                return ApiFormatter::createApi(400, 'Bad Request', ['harga_jual' => ['Harga jual harus lebih besar dari harga beli']]);
            }
            
            $diskon = $request->filled('diskon') ? $request->validate(['diskon' => 'min:1'], 
                        [ 'diskon.min' => 'Diskon harus angka positif'])
                        ['diskon'] : $barang->diskon;
            
            if ($diskon >= $harga_jual) {
                return ApiFormatter::createApi(400, 'Bad Request', ['diskon' => ['Diskon harus lebih kecil dari harga jual']]);
            }
            $stok = $request->filled('stok') ? $request->validate(['stok' => 'min:1'], 
                    [ 'stok.min' => 'Stok harus angka positif'])
                    ['stok'] : $barang->stok;

            $barang->update(compact('supplier_id','kategori_barang_id','nama', 'foto', 'harga_beli', 'harga_jual', 'diskon', 'stok'));

            // $jenis = $request->filled('jenis') ? $request->validate(['jenis' => 'in:penambahan,pengurangan'], 
            // [  'jenis.in' => 'Jenis hanya diisi penambahan atau pengurangan'])
            // ['jenis'] : $request->jenis;

            /* update stok barang
            ->jika tidak ada request perubahan di stok maka ambil stok barang dari db
                ->di tabel riwayat stok barang maka update juga 
                ->stok_sebelum diambil dari field stok di tabel barang
                ->field stok_update diambil dari jika field jenis berupa penambahan maka stok_sebelum + request stok barang
                    ->dan jika field jenisnya berupa pengurangan maka stok_sebelum - request stok barang
                    ->dan jika stok_sebelumnya == request stok barang */
            // $riwayatStokBarangs = RiwayatStokBarang::find($barang->id);
            // $stok_sebelum = $barang->stok;
            // $jenis = $riwayatStokBarangs->stok_sebelum > $request->stok ? 'penambahan' : 'pengurangan';
            
            // // $jenis = $request->jenis;
            // if($riwayatStokBarangs->stok_sebelum != $stok){
            //     if($jenis != 'penambahan'){
            //        $stok_update =  $riwayatStokBarangs->stok_sebelum - $stok;
            //     }else{
            //         $stok_update =  $riwayatStokBarangs->stok_sebelum + $stok;
            //     }
            // }else{
            //     $stok_update = 0;
            // }
            // $stok_update = $barang->stok != $stok ? $barang->stok - $stok : 0;
            // $riwayat = RiwayatStokBarang::where('barang_id','=',$id)->select('keterangan')->get();
            // $keterangan = ($request->keterangan == null)  ? $riwayat : $request->keterangan ;
            

            // if($jenis == 'penambahan'){
            //     $stok_terakhir = $stok_update + $stok_sebelum;
            // } else if($jenis == 'pengurangan'){
            //     $stok_terakhir = $stok_sebelum - $stok_update;
            // }           

            // DB::table('riwayat_stok_barang')
            // ->where('barang_id', '=', $id)->update([
            //     'barang_id' => $barang->id,
            //     'stok_sebelum' => $stok_sebelum,
            //     'stok_terakhir' => $stok_terakhir,
            //     'jenis' => $jenis,
            //     'stok_update' => $stok_update,
            // ]);

            // $barangs = Barang::findOrFail($id);
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch(ValidationException $e){
            return ApiFormatter::createApi(400, 'Error', $e->errors());
        }catch (\Exception $e) {
            DB::rollback();
           return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        DB::commit();

        // if($stok_terakhir > 1){
        //     DB::table('barang')->where('id', '=', $barang->id)
        // ->update(['stok' => $stok_terakhir]);
        // }
        return ApiFormatter::createApi(200, 'OK', 'Sukses', $barang);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $barang = Barang::findOrFail($id);

            $foto = $barang->foto;
            $foto = explode('/', $foto);
            $foto = $foto[6];
            $path_foto = public_path('storage/img/barang/'. $foto);
            if(file_exists($path_foto)){
                unlink($path_foto);
            }

            $barang = $barang->delete();
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e){
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        return ApiFormatter::createApi(200, 'OK', 'Sukses menghapus data', $barang);        
    }
}
