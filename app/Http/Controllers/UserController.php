<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $show = $request->show;
            $q = $request->q;
            if($show){
                $show = User::select(['id', 'nama', 'username', 'no_telp'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('username', 'like', "%{$q}%")
                ->orWhere('no_telp', 'like', "%{$q}%")
                ->orderBy('nama', 'asc')                
                ->paginate($show, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            } else{
                $data = User::select(['id', 'nama', 'username', 'no_telp'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('username', 'like', "%{$q}%") 
                ->orWhere('no_telp', 'like', "%{$q}%") 
                ->orderBy('nama', 'asc')            
                ->paginate(5, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $data);   
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e) {
            return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
         } 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
            'no_telp' => 'required'
        ], [
            'nama.required' => 'Nama wajib diisi',
            'username.required' => 'Username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'no_telp.required' => 'No Telepon wajib diisi'
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

        // insert to table
        try {
            $user = User::create([
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'no_telp' => $request->no_telp
            ]);

            $payload = [
                'username' => 'admin',
                'nama' => 'admin'
            ];
            $jwt = JWT::encode($payload, UserController::$SECRET_KEY, 'HS256');
            setcookie('X-Minimarket', $jwt);

        } catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch (Exception $e) {
            return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        // response
        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $user);
    }

    public function login(Request $request){
        //Validasi (body)
        $validator = Validator::make($request->all(),[
            'username' => 'required',
            'password' => 'required'
        ], [
            'username.required' => 'Username wajib diisi',
            'password.required' => 'Password wajib diisi'
        ]);
        
        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }
        
        // validasi (cek username & password di db, jika tidak ada maka return error 404)
        try {
            $user = User::where('username', '=', $request->username)->first();

            if(!$user) {
                throw new Exception('Akun tidak ditemukan');
            }
    
            if(!Hash::check($request->password, $user->password)) {
                throw new Exception('Akun tidak ditemukan');
            }
        } catch(Exception $e) {
            return ApiFormatter::createApi(404, 'Error', 'Akun tidak ditemukan');
        }

        // [
        //     'message' => 'Token masih valid',
        //     'data' => [
        //         'previous_end_point' => '/api/barang'
        //     ]
        // ]

        // generate jwt
        $payload = [
            'username' => $user->username,
            'nama' => $user->nama
        ];

        // cek jwt
        try{
            $jwt = JWT::encode($payload, env('JWT_SECRET_KEY'), 'HS256');    
            setcookie('token', $jwt, time() + 1800, httponly:true);

            return ApiFormatter::createApi(200, 'Sukses', 'Login berhasil', [
                'username' => $payload['username'],
                'nama' => $payload['nama']
            ]);
        } catch(Exception $e){
            return ApiFormatter::createApi(500, 'Error', 'Gagal login');
        }

        // middleware
        // user (client) -> api barang -> response kirim balik ke user
        // user (client) -> middleware (cek login / cek token di cookies) -> jika sukses -> api barang -> response kirim ke user
    }                                              // -> jika gagal -> suruh login ulang

    public function logout(){
        try{
            setcookie("token", "", time() - 3600);
        }catch(Exception $e){
            return $e->getMessage();
        }
     
        return ApiFormatter::createApi(200, 'Success', 'Berhasil logout');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $user = User::findOrFail($id);
        } catch(\Exception $e) {
            // return $e->getMessage();
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validasi
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
            'no_telp' => 'required'
        ], [
            'nama.required' => 'Nama wajib diisi',
            'username.required' => 'Username wajib diisi',
            'password.required' => 'Password wajib diisi',
            'no_telp.required' => 'No Telepon wajib diisi'
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

        try {
            $user = User::findOrFail($id);
            
            $user->update([
                'nama' => $request->nama,
                'username' => $request->username,
                'password' => Hash::make($request->password),
                'no_telp' => $request->no_telp
            ]);
            } catch(ModelNotFoundException $e) {
                return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
            } catch (\Exception $e){
                //return $e->getMessage();
                return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
            }
    
            return ApiFormatter::createApi(200, 'OK', 'sukses', $user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            $user = $user->delete();
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e){
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        return ApiFormatter::createApi(200, 'OK', 'Sukses menghapus data', $user); 
    }
}
