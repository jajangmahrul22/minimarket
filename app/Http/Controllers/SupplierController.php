<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function total_supplier(){
        try{
            $supplier = Supplier::count();
        }catch (\Exception $e){
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        return ApiFormatter::createApi(200, 'OK', 'sukses', $supplier);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $show = $request->show;
            $q = $request->q;
            if($show){
                $show = Supplier::select(['id', 'nama', 'alamat', 'no_hp'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('alamat', 'like', "%{$q}%")
                ->orWhere('no_hp', 'like', "%{$q}%")
                ->orderBy('nama', 'asc')                
                ->paginate($show, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            } else{
                $data = Supplier::select(['id', 'nama', 'alamat', 'no_hp'])
                ->where('nama', 'like' , "%{$q}%")
                ->orWhere('id', 'like', "%{$q}%")
                ->orWhere('alamat', 'like', "%{$q}%") 
                ->orWhere('no_hp', 'like', "%{$q}%") 
                ->orderBy('nama', 'asc')            
                ->paginate(5, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $data);   
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e) {
            // return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
         }   
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Validasi
        $validator = Validator::make($request->all(),[
            'nama' => 'required',
            'alamat' => 'required',
            'no_hp' => 'required|numeric'
        ], [
            'nama.required' => 'Nama wajib diisi',
            'alamat.required' => 'Alamat wajib diisi',
            'no_hp.required' => 'No Hp wajib diisi',
            'no_hp.numeric' => 'No Hp harus berupa angka'
        ]);

        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }
        if (!$request->filled('no_hp')) {
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp wajib diisi']]);
        }
        $noHp = $request->input('no_hp');
        /* if(substr($noHp, 0, 1) !== '0' && strlen($noHp) < 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => 'No Hp harus terdiri dari 12 angka dan dimulai dari angka 0']);
        }else */ 
        if(substr($noHp, 0, 1) !== '0'){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus dimulai dengan angka 0']]);
        }else if(strlen($noHp) < 12 || strlen($noHp) > 12){
            return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus terdiri dari 12 angka']]);
        }

     

        // insert to table
        try {
            $supplier = Supplier::create([
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'no_hp' => $request->no_hp
            ]);
        } catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch (\Exception $e) {
            // return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        // response
        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $supplier);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $supplier = Supplier::findOrFail($id);
        } catch(\Exception $e) {
            // return $e->getMessage();
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $supplier);
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Validasi
        // $validator = Validator::make($request->all(),[
        //     'nama' => 'nullable',
        //     'alamat' => 'nullable',
        //     'no_hp' => 'regex:/^0[0-9]{11}$/'
        // ], [
        //     'no_hp.regex' => 'Format No Hp harus diawali angka 0 dan terdiri dari 12 angka ',
        // ]);

        // if ($validator->fails()) {
        //     return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        // }

        try {
            $supplier = Supplier::findOrFail($id);

            $nama = $request->filled('nama') ? $request->nama : $supplier->nama;
            $alamat = $request->filled('alamat') ? $request->alamat : $supplier->alamat;
            $no_hp = $request->filled('no_hp') ? $request->validate(['no_hp' => 'nullable|numeric'],[
                'no_hp.numeric' => 'No Hp harus berupa angka'
            ])
            ['no_hp'] : $supplier->no_hp;
            /* if(substr($no_hp, 0, 1) !== '0' && strlen($no_hp) < 12){
                return ApiFormatter::createApi(400, 'Error', ['no_hp' => 'No Hp harus terdiri dari 12 angka dan dimulai dari angka 0']);
            }else */ 
            if(substr($no_hp, 0, 1) !== '0'){
                return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus dimulai dengan angka 0']]);
            }else if(strlen($no_hp) < 12 || strlen($no_hp) > 12){
                return ApiFormatter::createApi(400, 'Error', ['no_hp' => ['No Hp harus terdiri dari 12 angka']]);
            }
            $supplier->update(compact('nama','no_hp', 'alamat'));
            } catch(ModelNotFoundException $e) {
                return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
            }catch(ValidationException $e){
                return ApiFormatter::createApi(400, 'Error', $e->errors());
            } catch (\Exception $e){
                //return $e->getMessage();
                return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
            }
    
            return ApiFormatter::createApi(200, 'OK', 'sukses', $supplier);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $supplier = Supplier::findOrFail($id);
            // hapus data supplier
            $supplier->delete();
        } catch(ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        } catch(\Exception $e) {
            return ApiFormatter::createApi(500, 'Error', 'Data gagal dihapus');
        }

        return ApiFormatter::createApi(200, 'OK', 'Sukses menghapus data');        
    }
}
