<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Barang;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use App\Models\KategoriBarang;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class KategoriBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function total_kategori(){
        try{
            // $kategori = DB::table('kategori_barang')
            // ->select(DB::raw('count(id) as total_data_kategori'))->get();
            $kategori = KategoriBarang::count();
        }catch (\Exception $e){
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        return ApiFormatter::createApi(200, 'OK', 'sukses', $kategori);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $show = $request->show;
            $q = $request->q;
            if ($show){
                $show = KategoriBarang::select(['id', 'nama_kategori'])
                            ->where('id', 'like', "%{$q}%")
                            ->orWhere('nama_kategori', 'like', "%{$q}%")
                            ->orderBy('nama_kategori', 'asc')
                            ->paginate($show, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $show);
            }else {
                $data = KategoriBarang::select(['id', 'nama_kategori'])
                            ->where('id', 'like', "%{$q}%")
                            ->orWhere('nama_kategori', 'like', "%{$q}%")
                            ->orderBy('nama_kategori', 'asc')
                            ->paginate(5, ['*'], 'p');
                return ApiFormatter::createApi(200, 'OK', 'sukses', $data);
            }
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e){
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'required'
        ],[
            'nama_kategori.required' => 'Kategori wajib diisi'
        ]);
        
        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }
        
        try{            
            $kategori = KategoriBarang::create([
                'nama_kategori' => $request->nama_kategori,
                // 'id_barang' => $request->id_barang
            ]);
            // $kategori = DB::table('kategori_barang')
            // ->leftJoin('barang', 'kategori_barang.id_barang', '=', 'barang.id')
            // ->select('kategori_barang.id as id', 'kategori_barang.nama_kategori as nama_kategori')
            // ->get(); 
            //return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $id_barang);
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (Exception $e) {
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        
        return ApiFormatter::createApi(201, 'Created', 'Data berhasil ditambahkan', $kategori);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $kategori = KategoriBarang::findOrFail($id);
        }catch (\Exception $e){
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }

        return ApiFormatter::createApi(200, 'OK', 'sukses', $kategori);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_kategori' => 'nullable'
        ],[
            'nama_kategori.nullable' => 'Kategori wajib diisi'
        ]);
        
        if ($validator->fails()) {
            return ApiFormatter::createApi(400, 'Error', $validator->messages()->get('*'));
        }

        try{        
            $kategori = KategoriBarang::findOrFail($id);

            $nama_kategori = $request->nama_kategori;
            // $id_barang = $request->id_barang;
            if ($nama_kategori != null){
               $nama_kategori  = $nama_kategori;
            //    $id_barang = $id_barang;
            }else {
                $nama_kategori = $kategori->nama_kategori;
                // $id_barang = $kategori->id;
            }

            $kategori->update([
                'nama_kategori' => $nama_kategori,
                // 'id_barang' => $id_barang
            ]);
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (Exception $e) {
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }
        
        return ApiFormatter::createApi(200, 'sukses', $kategori);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kategori = KategoriBarang::findOrFail($id);
            $kategori = $kategori->delete();
        }catch (ModelNotFoundException $e) {
            return ApiFormatter::createApi(404, 'Not found', 'Data tidak ditemukan');
        }catch (\Exception $e){
            //return $e->getMessage();
            return ApiFormatter::createApi(500, 'Error', 'Ups! Ada yang tidak beres');
        }

        return ApiFormatter::createApi(200, 'OK', 'Sukses menghapus data', $kategori); 
    }
}
