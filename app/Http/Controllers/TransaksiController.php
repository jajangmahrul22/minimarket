<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Barang;
use App\Models\Transaksi;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Helpers\ApiFormatter;
use App\Models\DetailTransaksi;
use App\Models\RiwayatStokBarang;
use Illuminate\Support\Facades\DB;

class TransaksiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        DB::beginTransaction();

        try{
            //input transaksi
            $transaksiRequest = [
                'no_struk' => Str::random(12),
                'tanggal_input' =>  $request->tanggal_input ? $request->tanggal_input : now(),
                'bayar' => $request->bayar ?? null,
                'diskon' => $request->diskon ?? null,
            ];

            $transaksi = Transaksi::create($transaksiRequest);

            //input detail transaksi
            $detail_transaksi = $request->detail;

            for($i = 0; $i < count($detail_transaksi); $i++) {
                $detail = $detail_transaksi[$i];
                $barang_id = $detail['barang_id'];
                $qty = $detail['qty'];

                $barang = Barang::find($barang_id);

                if(empty($barang)){
                    DB::rollback();
                    return ApiFormatter::createApi(400, 'Error', 'Barang dengan id tersebut tidak tersedia');
                }

                if ($qty > $barang->stok) {
                    DB::rollback();
                    return ApiFormatter::createApi(400, 'Error', 'Quantity melebihi stok barang yang tersedia');
                }

                $detail_diskon = $barang->diskon ?? 0;
                DetailTransaksi::create([
                    'transaksi_id' => $transaksi->id,
                    'nama_barang' => $barang->nama,
                    'harga_barang' => $barang->harga_jual,
                    'qty' => $qty,
                    'diskon' => $detail_diskon * $qty,
                    'total_harga' => ($barang->harga_jual * $qty) - ($detail_diskon * $qty)
                ]);
                
                $barang->update([
                    'stok' => $barang->stok - $qty
                ]);

                $riwayat_transaksi = [
                    'barang_id' => $barang_id,
                    'stok_sebelum' => $barang->stok + $qty,
                    'stok_update' => $qty,
                    'jenis' => 'pengurangan'
                ];
                $riwayat_transaksi = RiwayatStokBarang::create($riwayat_transaksi);

                $riwayat_transaksi->update([
                    'stok_terakhir' => $riwayat_transaksi->stok_sebelum - $riwayat_transaksi->stok_update,
                    'keterangan' => 'Transaksi berhasil'
                ]);
            }

            $qty_transaksi = DetailTransaksi::where('transaksi_id', '=', $transaksi->id)->sum('qty');
            $total_harga_transaksi = DetailTransaksi::where('transaksi_id', '=', $transaksi->id)->sum('total_harga');
            $transaksi->update([
                'qty' => $qty_transaksi,
                'total_harga' => $total_harga_transaksi,
                'grand_total_harga' => $grand_total_harga = $total_harga_transaksi - $transaksi->diskon,
                'kembalian' => $transaksi->bayar - $grand_total_harga
            ]);

            if ($transaksi->bayar < $transaksi->grand_total_harga) {
                DB::rollback();
                return ApiFormatter::createApi(400, 'Error', 'Uang pembayaran kurang');
            }

            $detail = DB::table('transaksi')
                ->join('detail_transaksi', 'detail_transaksi.transaksi_id', '=', 'transaksi.id')
                ->select('nama_barang', 'detail_transaksi.harga_barang as harga_satuan','detail_transaksi.qty', 
                'detail_transaksi.diskon', 'detail_transaksi.total_harga as total_harga_barang')
                ->where('transaksi.id', '=', $transaksi->id)
                ->get();
        }catch(Exception $e){
            // return $e->getMessage();
            DB::rollback();
            return ApiFormatter::createApi(500, 'Error', $e->getMessage());
        }
        DB::commit();

        $response = [
            'tanggal_input' => $transaksi->tanggal_input,
            'qty' => $transaksi->qty,
            'total_harga' => $transaksi->total_harga,
            'diskon' => $transaksi->diskon,
            'grand_total_harga' => $transaksi->grand_total_harga,
            'bayar' => $transaksi->bayar,
            'kembalian' => $transaksi->kembalian,
            'detail' => $detail
        ];
        return ApiFormatter::createApi(201, 'Created', 'Transaksi berhasil', $response);
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
