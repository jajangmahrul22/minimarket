<?php

namespace App\Models;

use App\Models\Supplier;
use App\Models\KategoriBarang;
use App\Models\RiwayatStokBarang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Barang extends Model
{
    use HasFactory;
    protected $table = 'barang';
    protected $guarded = [];

    protected $fillable = [
        'supplier_id',
        'kategori_barang_id',
        'foto',
        'nama',
        'harga_beli',
        'harga_jual',
        'diskon',
        'stok'
    ];

    // public function kategori_barang()
    // {
    //     return $this->hasMany(KategoriBarang::class);
    // }
    public function supplier()
    {
        return $this->belongsTo(Supplier::class);
    }

    public function kategoriBarang()
    {
        return $this->belongsTo(KategoriBarang::class);
    }
    public function riwayat_stok_barang()
    {
        return $this->hasMany(RiwayatStokBarang::class);
    }

    protected $hidden = [];
}
