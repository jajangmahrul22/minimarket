<?php

namespace App\Models;

use App\Models\Barang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Supplier extends Model
{
    use HasFactory;

    protected $table = 'supplier';
    protected $fillable = [
        'nama',
        'alamat',
        'no_hp'
    ];

    public function barang()
    {
        return $this->hasMany(Barang::class);
    }

    public function kategori_barang()
    {
        return $this->belongsTo(KategoriBarang::class);
    }

    protected $hidden = ['created_at', 'updated_at'];
}
