<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $table = 'transaksi';
    protected $guarded = [];

    protected $fillable = [
        'no_struk',
        'qty',
        'total_harga',
        'diskon',
        'grand_total_harga',
        'bayar',
        'kembalian',
        'tanggal_input'
    ];

    protected $hidden = [];
}
