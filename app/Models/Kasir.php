<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Kasir extends Model
{
    use HasFactory;
    // use SoftDeletes;

    protected $table = 'kasir';
    protected $fillable = [
        'foto',
        'nama',
        'umur',
        'no_hp',
        'email',
        'alamat',
        'status',
        'tgl_mulai_bekerja'
    ];

    protected $hidden = [];
}
