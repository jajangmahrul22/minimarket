<?php

namespace App\Models;

use App\Models\Transaksi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DetailTransaksi extends Model
{
    use HasFactory;
    protected $table = 'detail_transaksi';
    protected $guarded = [];

    protected $fillable = [
        'transaksi_id',
        'barang_id',
        'nama_barang',
        'harga_barang',
        'qty',
        'diskon',
        'total_harga',
    ];

    public function transaksi()
    {
        return $this->hasMany(Transaksi::class);
    }

    protected $hidden = [];
}
