<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RiwayatStokBarang extends Model
{
    use HasFactory;

    protected $table = 'riwayat_stok_barang';
    protected $guarded = [];

    protected $fillable = [
        'barang_id',
        'stok_sebelum',
        'stok_terakhir',
        'jenis',
        'stok_update',
        'keterangan'
    ];

    protected $hidden = ['updated_at'];
}
